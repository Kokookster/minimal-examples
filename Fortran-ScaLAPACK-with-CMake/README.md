# ScaLAPACK Minimal Example

This is a minimal build example for Fortran code that uses ScaLAPACK.
The Fortran source code is copied from:
<http://www.netlib.org/scalapack/examples/example1.f>

Very helpful information about ScaLAPACK can be found on:
<http://netlib.org/scalapack/slug/scalapack_slug.html>

## Setting up conda environment

```
conda create -n scalapack_env
conda activate scalapack_env
conda install -c conda-forge cmake gfortran openmpi-mpifort scalapack
```

## CMake initial cache file

Create folder `build` and put in the following initial chache file `cmake.anaconda`:

```
set(CMAKE_Fortran_COMPILER "mpif90" CACHE STRING "")
set(CMAKE_Fortran_FLAGS "-O0" CACHE STRING "")
set(CMAKE_PREFIX_PATH "${CONDA_PREFIX}")
```

## Compiling

From the `build` directory execute:

```
cmake -C cmake.anaconda ../.
make
```

## Running

To run the example execute:

```
mpirun -n 6 example1
```

On systems using SLURM job scheduler you might need to allocate resources, first, similar to (e.g. on MPCDF COBRA system):

```
salloc --time=0:05:00 --mem=3000 -p interactive -n 6
mpirun -n 6 example1
```
